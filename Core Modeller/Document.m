//
//  Document.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "Document.h"
#import "NSManagedObjectModel+momcom.h"
#import "GDFUtilities.h"
#import "SHOPCoreData.h"

@interface Document ()

@end

@implementation Document {
	NSXMLDocument *_document;
}

- (NSString *)currentModelVersionAtPath:(NSString *)xcdatamodeldPath error:(NSError **)error
{
	NSString *currentVersionName = nil;
	NSArray *xcdatamodeldContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:xcdatamodeldPath error:error];
	if ([xcdatamodeldContents count] == 0) {
		return nil;
	}
	for (NSString *filename in xcdatamodeldContents) {
		NSString *fullPath = [xcdatamodeldPath stringByAppendingPathComponent:filename];
		if ([filename isEqualToString:@".xccurrentversion"]) {
			NSDictionary *versionInfo = [NSDictionary dictionaryWithContentsOfFile:fullPath];
			//currentVersionName = [[versionInfo objectForKey:@"_XCCurrentVersionName"] stringByDeletingPathExtension];
			currentVersionName = [versionInfo objectForKey:@"_XCCurrentVersionName"];
			break;
		} else if ([filename hasSuffix:@".xcdatamodel"]) {
			currentVersionName = filename;
			break;
		} else if ([filename isEqual:@"contents"]) {
			currentVersionName = @"";
			break;
		}
	}
	return [currentVersionName stringByAppendingPathComponent:@"contents"];
}

- (BOOL)readFromURL:(NSURL *)url ofType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError
{
	NSError *xmlError;
	NSString *xmlFilePath = [self currentModelVersionAtPath:url.path error:&xmlError];
	if (xmlError) {
		GDFErrorSet(outError, xmlError);
		return NO;
	}
	
	_document = [[NSXMLDocument alloc] initWithContentsOfURL:[url URLByAppendingPathComponent:xmlFilePath] options:0 error:&xmlError];
	if (xmlError) {
		GDFErrorSet(outError, xmlError);
		return NO;
	}
	
	NSError *compileError;
	NSString *compiledModelPath = [NSManagedObjectModel compileModelAtPath:url.path inDirectory:NSTemporaryDirectory() error:&compileError];
	if (compiledModelPath) {
		_model = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL URLWithString:compiledModelPath]];
	}
	GDFErrorSet(outError, compileError);
	return _model != nil;
}

+ (BOOL)autosavesInPlace
{
	return YES;
}

- (void)makeWindowControllers {
	// Override to return the Storyboard file name of the document.
	[self addWindowController:[[NSStoryboard storyboardWithName:@"Main" bundle:nil] instantiateControllerWithIdentifier:@"Document Window Controller"]];
}

- (void)saveToURL:(NSURL *)url ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation completionHandler:(void (^)(NSError * __nullable errorOrNil))completionHandler
{
	for (NSEntityDescription *entity in self.model.entities) {
		[self addUserInfoToEntity:entity];
	}
}

- (NSArray <NSXMLElement *> *)userInfoXMLElements:(NSDictionary <NSString *, NSString *> *)userInfo
{
	NSMutableArray *userInfoElements = [[NSMutableArray alloc] init];
	[userInfo enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
		NSXMLElement *element = [NSXMLElement elementWithName:@"entry"];
		[element setAttributesWithDictionary:@{@"key" : key, @"value" : obj}];
		[userInfoElements addObject:element];
	}];
	return [userInfoElements copy];
}

- (NSXMLElement *)userInfoElementForXPath:(NSString *)xpath
{
	NSError *xpathError;
	NSArray <__kindof NSXMLNode *> *nodes = [_document nodesForXPath:xpath error:&xpathError];
	NSXMLElement *parentElement = GDFDynamicCast(nodes.firstObject, NSXMLElement);
	NSXMLElement *userInfoElement = GDFDynamicCast([parentElement nodesForXPath:@"userInfo" error:nil].firstObject, NSXMLElement);
	if (userInfoElement == nil) {
		userInfoElement = [NSXMLElement elementWithName:@"userInfo"];
		[parentElement addChild:userInfoElement];
	}
	return userInfoElement;
}

- (void)addUserInfoToEntity:(NSEntityDescription *)entity
{
	NSString *xpath = [NSString stringWithFormat:@"model/entity[@name=\"%@\"]", entity.name];
	NSXMLElement *userInfoElement = [self userInfoElementForXPath:xpath];
	[userInfoElement setChildren:[self userInfoXMLElements:entity.userInfo]];
	
	for (NSPropertyDescription *property in entity.properties) {
		[self addUserInfoToProperty:property];
	}
}

- (void)addUserInfoToProperty:(NSPropertyDescription *)property
{
	NSString *xpath = [NSString stringWithFormat:@"model/entity[@name=\"%@\"]/%@[@name=\"%@\"", property.entity.name, property.XMLName, property.name];
	NSXMLElement *userInfoElement = [self userInfoElementForXPath:xpath];
	[userInfoElement setChildren:[self userInfoXMLElements:property.userInfo]];
}

@end
