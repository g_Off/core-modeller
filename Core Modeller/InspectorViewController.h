//
//  InspectorViewController.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-23.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface InspectorViewController : NSViewController

@property (nonatomic, copy) NSArray <__kindof NSViewController *> *inspectors;

@end
