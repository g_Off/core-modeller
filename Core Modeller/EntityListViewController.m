//
//  EntityListViewController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "EntityListViewController.h"
#import "GDFUtilities.h"

@interface EntityListViewController () <NSTableViewDataSource, NSTableViewDelegate>

@property (weak) IBOutlet NSTableView *tableView;

@end

@implementation EntityListViewController {
	NSMutableDictionary *_relationships;
	NSMutableDictionary *_attributes;
}

- (void)setEntities:(NSArray<NSEntityDescription *> *)entities
{
	_entities = [entities sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
	
	if (self.viewLoaded) {
		[self.tableView reloadData];
	}
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
	return self.entities.count;
}

- (nullable id)tableView:(NSTableView *)tableView objectValueForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
	return self.entities[row];
}

#pragma mark - NSTableViewDelegate

- (nullable NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
	NSTableCellView *cell = [tableView makeViewWithIdentifier:@"EntityCell" owner:self];
	cell.textField.stringValue = _entities[row].name;
	return cell;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
	NSEntityDescription *entity;
	NSInteger row = self.tableView.selectedRow;
	if (row >= 0 && row < self.entities.count) {
		entity = self.entities[row];
	}
	[self.delegate entityListViewController:self didSelectEntity:entity];
}

@end
