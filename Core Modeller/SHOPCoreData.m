//
//  SHOPCoreData.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "SHOPCoreData.h"

const struct SHOPCoreDataJSONKeys SHOPCoreDataJSONKeys = {
	.key = @"json:key",
	.transformer = @"json:transformer",
	.transformerMethod = @"json:transformerMethod",
	.required = @"json:required",
	.remoteId = @"json:remoteId",
	.name = @"json:name",
	.automaticallyMapped = @"json:automaticallyMapped",
	.excluded = @"json:excluded",
	.prefetch = @"json:prefetch",
};

@interface NSDictionary (SHOPCoreDataJSONDeserializer)

- (instancetype)dictionaryByReplacingValue:(id)value forKey:(id)key;

@end

@implementation NSDictionary (SHOPCoreDataJSONDeserializer)

- (instancetype)dictionaryByReplacingValue:(id)value forKey:(id)key
{
	NSMutableDictionary *d = [self mutableCopy];
	if (value) {
		d[key] = value;
	} else {
		[d removeObjectForKey:key];
	}
	return [d copy];
}

@end

@implementation NSEntityDescription (SHOPCoreDataJSONDeserializer)

- (NSPropertyDescription *)JSONRemoteId
{
	NSString *serverKeyName = self.userInfo[SHOPCoreDataJSONKeys.remoteId];
	return self.propertiesByName[serverKeyName];
}

- (void)setJSONRemoteId:(NSPropertyDescription *)JSONRemoteId
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:JSONRemoteId.name forKey:SHOPCoreDataJSONKeys.remoteId];
}

- (NSString *)JSONName
{
	return self.userInfo[SHOPCoreDataJSONKeys.name];
}

- (void)setJSONName:(NSString *)JSONName
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:JSONName.length > 0 ? JSONName : nil forKey:SHOPCoreDataJSONKeys.name];
}

- (BOOL)isJSONAutomaticallyMapped
{
	return [self.userInfo[SHOPCoreDataJSONKeys.automaticallyMapped] boolValue];
}

- (void)setJSONAutomaticallyMapped:(BOOL)JSONAutomaticallyMapped
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(JSONAutomaticallyMapped ? @"YES" : nil) forKey:SHOPCoreDataJSONKeys.automaticallyMapped];
}

- (BOOL)JSONPrefetch
{
	return [self.userInfo[SHOPCoreDataJSONKeys.prefetch] boolValue];
}

- (void)setJSONPrefetch:(BOOL)JSONPrefetch
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(JSONPrefetch ? @"YES" : nil) forKey:SHOPCoreDataJSONKeys.prefetch];
}

@end

@implementation NSPropertyDescription (SHOPCoreDataJSONDeserializer)

- (NSString *)JSONKey
{
	NSDictionary *userInfo = self.userInfo;
	NSString *JSONKey = userInfo[SHOPCoreDataJSONKeys.key];
	
	if (JSONKey.length == 0 && self.entity.JSONAutomaticallyMapped && self.JSONExcluded == NO) {
		NSString *name = self.name;
		if ([name isEqual:@"remoteId"]) {
			JSONKey = @"id";
		} else if ([name isEqual:[self.entity.JSONName stringByAppendingString:name.capitalizedString]]) {
			JSONKey = @"description";
		} else {
			JSONKey = self.name.snakeCase;
		}
	}
	
	return JSONKey;
}

- (void)setJSONKey:(NSString *)JSONKey
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:JSONKey forKey:SHOPCoreDataJSONKeys.key];
}

- (NSString *)JSONValueTransformer
{
	return self.userInfo[SHOPCoreDataJSONKeys.transformer];
}

- (void)setJSONValueTransformer:(NSString *)JSONValueTransformer
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:JSONValueTransformer forKey:SHOPCoreDataJSONKeys.transformer];
}

- (NSString *)JSONValueTransformerMethod;
{
	return self.userInfo[SHOPCoreDataJSONKeys.transformerMethod];
}

- (void)setJSONValueTransformerMethod:(NSString *)JSONValueTransformerMethod
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:JSONValueTransformerMethod forKey:SHOPCoreDataJSONKeys.transformerMethod];
}

- (BOOL)isJSONExcluded
{
	return [self.userInfo[SHOPCoreDataJSONKeys.excluded] boolValue];
}

- (void)setJSONExcluded:(BOOL)JSONExcluded
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(JSONExcluded ? @"YES" : nil) forKey:SHOPCoreDataJSONKeys.excluded];
}

- (BOOL)isJSONRequired
{
	return [self.userInfo[SHOPCoreDataJSONKeys.required] boolValue];
}

- (void)setJSONRequired:(BOOL)JSONRequired
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(JSONRequired ? @"YES" : nil) forKey:SHOPCoreDataJSONKeys.required];
}

- (BOOL)isReadOnly
{
	return [self.userInfo[@"mogenerator.readonly"] boolValue];
}

- (void)setReadOnly:(BOOL)readOnly
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(readOnly ? @"YES" : nil) forKey:@"mogenerator.readonly"];
}

- (NSString *)XMLName
{
	return nil;
}

@end

@implementation NSAttributeDescription (SHOPCoreDataJSONDeserializer)

- (NSString *)XMLName
{
	return @"attribute";
}

@end

@implementation NSRelationshipDescription (SHOPCoreDataJSONDeserializer)

- (BOOL)JSONPrefetch
{
	return [self.userInfo[SHOPCoreDataJSONKeys.prefetch] boolValue];
}

- (void)setJSONPrefetch:(BOOL)JSONPrefetch
{
	self.userInfo = [self.userInfo dictionaryByReplacingValue:(JSONPrefetch ? @"YES" : nil) forKey:SHOPCoreDataJSONKeys.prefetch];
}

- (NSString *)XMLName
{
	return @"relationship";
}

@end

@implementation NSString (SHOPCoreDataJSONDeserializer)

- (NSString *)snakeCase
{
	NSMutableString *output = [NSMutableString string];
	NSCharacterSet *uppercaseCharacterSet = [NSCharacterSet uppercaseLetterCharacterSet];
	BOOL previousCharacterWasUppercase = NO;
	BOOL currentCharacterIsUppercase = NO;
	unichar currentChar = 0;
	unichar previousChar = 0;
	for (NSInteger idx = 0; idx < self.length; idx += 1) {
		previousChar = currentChar;
		currentChar = [self characterAtIndex:idx];
		previousCharacterWasUppercase = currentCharacterIsUppercase;
		currentCharacterIsUppercase = [uppercaseCharacterSet characterIsMember:currentChar];
		
		if (!previousCharacterWasUppercase && currentCharacterIsUppercase && idx > 0) {
			// insert an _ between the characters
			[output appendString:@"_"];
		} else if (previousCharacterWasUppercase && !currentCharacterIsUppercase) {
			// insert an _ before the previous character
			// insert an _ before the last character in the string
			if ([output length] > 1) {
				unichar charTwoBack = [output characterAtIndex:output.length - 2];
				if (charTwoBack != '_') {
					[output insertString:@"_" atIndex:output.length - 1];
				}
			}
		}
		// Append the current character lowercase
		[output appendString:[NSString stringWithCharacters:&currentChar length:1].lowercaseString];
	}
	return [output copy];
}

@end
