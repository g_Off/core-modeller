//
//  EntityInspectorViewController.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EntityInspectorViewController : NSViewController

@property (nonatomic, strong) NSEntityDescription *entity;

@end
