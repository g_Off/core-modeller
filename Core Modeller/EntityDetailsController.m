//
//  EntityDetailsController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-23.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "EntityDetailsController.h"

@interface EntityDetailsController () <NSTableViewDataSource, NSTableViewDelegate>

@property (weak) IBOutlet NSTableView *tableView;

@end

@implementation EntityDetailsController {
	NSArray <NSAttributeDescription *> *_attributes;
	NSArray <NSRelationshipDescription *> *_relationships;
}

- (void)setEntityDescription:(NSEntityDescription *)entityDescription
{
	_entityDescription = entityDescription;
	
	_attributes = [_entityDescription.attributesByName.allValues sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
	_relationships = [_entityDescription.relationshipsByName.allValues sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
	
	if (self.viewLoaded) {
		[self.tableView reloadData];
	}
}

- (NSIndexPath *)indexPathForTableRow:(NSInteger)tableRow
{
	NSIndexPath *indexPath;
	if (tableRow < 1 + _attributes.count) {
		indexPath = [NSIndexPath indexPathWithIndex:0];
		if (tableRow > 0) {
			indexPath = [indexPath indexPathByAddingIndex:(tableRow - 1)];
		}
	} else {
		tableRow -= (1 + _attributes.count);
		indexPath = [NSIndexPath indexPathWithIndex:1];
		if (tableRow > 0) {
			indexPath = [indexPath indexPathByAddingIndex:(tableRow - 1)];
		}
	}
	return indexPath;
}

- (__kindof NSPropertyDescription *)propertyAtIndexPath:(NSIndexPath *)indexPath
{
	NSPropertyDescription *property;
	if (indexPath.length == 2) {
		NSInteger row = [indexPath indexAtPosition:1];
		if ([indexPath indexAtPosition:0] == 0) {
			property = _attributes[row];
		} else {
			property = _relationships[row];
		}
	}
	return property;
}

- (NSString *)titleAtIndexPath:(NSIndexPath *)indexPath
{
	return ([indexPath indexAtPosition:0] == 0) ? @"Attributes" : @"Relationships";
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
	return _attributes.count + _relationships.count + 2;
}

- (nullable id)tableView:(NSTableView *)tableView objectValueForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
	NSIndexPath *indexPath = [self indexPathForTableRow:row];
	if (indexPath.length == 1) {
		if ([indexPath indexAtPosition:0] == 0) {
			return @"Attributes";
		} else {
			return @"Relationships";
		}
	} else {
		if ([indexPath indexAtPosition:0] == 0) {
			return _attributes[[indexPath indexAtPosition:1]];
		} else {
			return _relationships[[indexPath indexAtPosition:1]];
		}
	}
}

- (BOOL)tableView:(NSTableView *)tableView isGroupRow:(NSInteger)row
{
	return [self indexPathForTableRow:row].length == 1;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row
{
	return [self indexPathForTableRow:row].length != 1;
}

#pragma mark - NSTableViewDelegate

- (nullable NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row
{
	NSIndexPath *indexPath = [self indexPathForTableRow:row];
	NSString *name = indexPath.length > 1 ? [self propertyAtIndexPath:indexPath].name : [self titleAtIndexPath:indexPath];
	NSTableCellView *cell = [tableView makeViewWithIdentifier:@"PropertyCell" owner:self];
	cell.textField.stringValue = name;
	return cell;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
	NSIndexPath *indexPath = [self indexPathForTableRow:self.tableView.selectedRow];
	[self.delegate entityDetailsController:self didSelectPropertyDescription:[self propertyAtIndexPath:indexPath]];
	
}

@end
