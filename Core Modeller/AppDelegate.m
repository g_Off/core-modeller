//
//  AppDelegate.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}

@end
