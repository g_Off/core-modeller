//
//  GDFUtilities.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import <Foundation/Foundation.h>

/* Definition of `GDF_INLINE'. */

#if !defined(GDF_INLINE)
# if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define GDF_INLINE static inline
# elif defined(__cplusplus)
#  define GDF_INLINE static inline
# elif defined(__GNUC__)
#  define GDF_INLINE static __inline__
# else
#  define GDF_INLINE static
# endif
#endif

#define BLOCK_EXEC(block, ...) if (block) { block(__VA_ARGS__); };
#define DISPATCH_EXEC(queue, block, ...) if (block) { dispatch_async(queue, ^{ block(__VA_ARGS__); } ); }
#define MAIN_EXEC(block, ...) if (block) { dispatch_async(dispatch_get_main_queue(), ^{ block(__VA_ARGS__); } ); }

#define GDFErrorSet(errorptr, error) if (errorptr) { *errorptr = error; }

#define GDFSuppressPerformSelectorWarning(PerformCall) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
PerformCall; \
_Pragma("clang diagnostic pop") \
} while (0)

id GDFDynamicCast_(id x, Class objClass);

#define GDFDynamicCast(x, c) ((c *) GDFDynamicCast_(x, [c class]))

GDF_INLINE BOOL
GDFEqualObjects(id o1, id o2)
{
	return (o1 == o2) || (o1 && o2 && [o1 isEqual:o2]);
}
