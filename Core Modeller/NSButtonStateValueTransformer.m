//
//  NSButtonStateValueTransformer.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "NSButtonStateValueTransformer.h"
#import "GDFUtilities.h"

@import AppKit;

@implementation NSButtonStateValueTransformer

+ (Class)transformedValueClass
{
	return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
	return NO;
}

- (nullable id)transformedValue:(nullable id)value
{
	return @(GDFDynamicCast(value, NSNumber).integerValue == NSOnState);
}

@end
