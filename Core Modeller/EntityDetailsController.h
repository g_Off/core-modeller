//
//  EntityDetailsController.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-23.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

@import AppKit;
@import CoreData;

@class EntityDetailsController;

@protocol EntityDetailsControllerDelegate <NSObject>

- (void)entityDetailsController:(EntityDetailsController *)entityDetailsController didSelectPropertyDescription:(__kindof NSPropertyDescription *)propertyDescription;

@end

@interface EntityDetailsController : NSViewController

@property (weak) id <EntityDetailsControllerDelegate> delegate;
@property (nonatomic, strong) NSEntityDescription *entityDescription;

@end
