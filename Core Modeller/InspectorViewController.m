//
//  InspectorViewController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-23.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "InspectorViewController.h"
#import "InspectorHeaderView.h"

@interface InspectorViewController ()

@property (weak) IBOutlet NSStackView *stackView;

@end

@implementation InspectorViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self addAllInspectors];
}

- (void)setInspectors:(NSArray<__kindof NSViewController *> *)inspectors
{
	[self removeAllInspectors];
	_inspectors = [inspectors copy];
	[self addAllInspectors];
}

- (void)removeAllInspectors
{
	for (NSViewController *controller in _inspectors) {
		[controller removeFromParentViewController];
	}
	if (self.viewLoaded) {
		for (NSView *view in self.stackView.views) {
			[view removeFromSuperview];
		}
	}
}

- (void)addAllInspectors
{
	for (NSViewController *controller in _inspectors) {
		[self addChildViewController:controller];
		InspectorHeaderView *inspectorHeader = [InspectorHeaderView insepectorHeaderView];
		inspectorHeader.titleLabel.stringValue = controller.title;
		NSBox *line = [[NSBox alloc] init];
		line.boxType = NSBoxSeparator;
		line.translatesAutoresizingMaskIntoConstraints = NO;
		
		[self.stackView addView:inspectorHeader inGravity:NSStackViewGravityTop];
		[self.stackView addView:controller.view inGravity:NSStackViewGravityTop];
		[self.stackView addView:line inGravity:NSStackViewGravityTop];
	}
}

@end
