//
//  GDFUtilities.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "GDFUtilities.h"

id GDFDynamicCast_(id x, Class objClass)
{
	return [x isKindOfClass:objClass] ? x : nil;
}
