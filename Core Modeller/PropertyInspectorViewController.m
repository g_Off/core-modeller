//
//  PropertyInspectorViewController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "PropertyInspectorViewController.h"
#import "SHOPCoreData.h"

@interface PropertyInspectorViewController ()

@property (weak) IBOutlet NSButton *noneRadioButton;
@property (weak) IBOutlet NSButton *classRadioButton;
@property (weak) IBOutlet NSButton *methodRadioButton;

@end

@implementation PropertyInspectorViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		self.title = @"Property";
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSButton *button;
	if (self.property.JSONValueTransformer.length > 0) {
		button = self.classRadioButton;
	} else if (self.property.JSONValueTransformerMethod.length > 0) {
		button = self.methodRadioButton;
	} else {
		button = self.noneRadioButton;
	}
	button.state = NSOnState;
}

- (IBAction)valueTransformerMethodDidChange:(id)sender
{
}


@end
