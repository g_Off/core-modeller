//
//  InspectorHeaderView.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "InspectorHeaderView.h"

@implementation InspectorHeaderView

+ (instancetype)insepectorHeaderView
{
	NSNib *nib = [[NSNib alloc] initWithNibNamed:@"InspectorHeaderView" bundle:nil];
	NSArray *topLevelObjects;
	if (![nib instantiateWithOwner:nil topLevelObjects:&topLevelObjects]) {
		return nil;
	}
	for (id obj in topLevelObjects) {
		if ([obj isKindOfClass:self]) {
			return obj;
		}
	}
	return nil;
}

@end
