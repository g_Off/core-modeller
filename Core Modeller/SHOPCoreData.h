//
//  SHOPCoreData.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import <CoreData/CoreData.h>

extern const struct SHOPCoreDataJSONKeys {
	__unsafe_unretained NSString *key;
	__unsafe_unretained NSString *transformer;
	__unsafe_unretained NSString *transformerMethod;
	__unsafe_unretained NSString *required;
	__unsafe_unretained NSString *remoteId;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *automaticallyMapped;
	__unsafe_unretained NSString *excluded;
	__unsafe_unretained NSString *prefetch;
} SHOPCoreDataJSONKeys;

@interface NSEntityDescription (SHOPCoreDataJSONDeserializer)

@property (nonatomic, readwrite, nullable) NSPropertyDescription *JSONRemoteId;
@property (nonatomic, readwrite, nullable) NSString *JSONName;
@property (nonatomic, readwrite, getter=isJSONAutomaticallyMapped) BOOL JSONAutomaticallyMapped;
@property (nonatomic, readwrite) BOOL JSONPrefetch;

@end

@interface NSPropertyDescription (SHOPCoreDataJSONDeserializer)

@property (nonatomic, readwrite, nullable) NSString *JSONKey;
@property (nonatomic, readwrite, nullable) NSString *JSONValueTransformer;
@property (nonatomic, readwrite, nullable) NSString *JSONValueTransformerMethod;
@property (nonatomic, readwrite, getter=isJSONExcluded) BOOL JSONExcluded;
@property (nonatomic, readwrite, getter=isJSONRequired) BOOL JSONRequired;

@property (nonatomic, readwrite, getter=isReadOnly) BOOL readOnly;

@property (nonatomic, readonly) NSString *XMLName;

@end

@interface NSRelationshipDescription (SHOPCoreDataJSONDeserializer)

@property (nonatomic, readwrite) BOOL JSONPrefetch;

@end

@interface NSString (SHOPCoreDataJSONDeserializer)

@property (nonatomic, readonly) NSString *snakeCase;

@end