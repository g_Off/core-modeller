//
//  EntityListViewController.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

@import AppKit;
@import CoreData;

@class EntityListViewController;

@protocol EntityListViewControllerDelegate <NSObject>

- (void)entityListViewController:(EntityListViewController *)entityListController didSelectEntity:(NSEntityDescription *)entity;

@end

@interface EntityListViewController : NSViewController

@property (weak) id <EntityListViewControllerDelegate> delegate;
@property (nonatomic, copy) NSArray <NSEntityDescription *> *entities;

@end
