//
//  EntityInspectorViewController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-24.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "EntityInspectorViewController.h"
#import "SHOPCoreData.h"

@interface EntityInspectorViewController ()

@property (strong) IBOutlet NSArrayController *remoteAttributeArrayController;

@property NSArray <NSAttributeDescription *> *attributes;
@property NSAttributeDescription *remoteIdAttribute;
@property NSPredicate *includedAttributesPredicate;
@property NSArray <NSSortDescriptor *> *attributesSortDescriptors;

@end

@implementation EntityInspectorViewController

- (instancetype)init
{
	if ((self = [super initWithNibName:@"EntityInspectorViewController" bundle:nil])) {
		self.title = @"Entity";
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.includedAttributesPredicate = [NSPredicate predicateWithFormat:@"SELF.JSONExcluded = NO"];
	self.attributesSortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
	
	NSAttributeDescription *remoteId = (NSAttributeDescription *)_entity.JSONRemoteId;
	if (remoteId == nil) {
		NSUInteger idx = [self.attributes indexOfObjectPassingTest:^BOOL(NSAttributeDescription * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			return [obj.JSONKey isEqual:@"id"];
		}];
		if (idx != NSNotFound) {
			remoteId = [self.attributes objectAtIndex:idx];
		}
	}
	if (remoteId) {
		NSUInteger selectionIndex = [self.remoteAttributeArrayController.arrangedObjects indexOfObject:remoteId];
		self.remoteAttributeArrayController.selectionIndex = selectionIndex;
	}
}

- (void)setEntity:(NSEntityDescription *)entity
{
	_entity = entity;
	self.attributes = _entity.attributesByName.allValues;
}

@end
