//
//  Document.h
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSDocument

@property (nonatomic, readonly) NSManagedObjectModel *model;

@end
