//
//  ModelWindowController.m
//  Core Modeller
//
//  Created by Geoffrey Foster on 2015-08-22.
//  Copyright © 2015 Geoffrey Foster. All rights reserved.
//

#import "ModelWindowController.h"
#import "EntityListViewController.h"
#import "EntityDetailsController.h"
#import "InspectorViewController.h"

#import "EntityInspectorViewController.h"
#import "PropertyInspectorViewController.h"

#import "Document.h"

#import "GDFUtilities.h"

@interface ModelWindowController () <EntityListViewControllerDelegate, EntityDetailsControllerDelegate>

@property (nonatomic, readonly) EntityListViewController *entityListController;
@property (nonatomic, readonly) EntityDetailsController *entityDetailsController;
@property (nonatomic, readonly) InspectorViewController *inspectorController;

@end

@implementation ModelWindowController {
	NSEntityDescription *_currentEntity;
}

- (void)setDocument:(Document *)document
{
	[super setDocument:document];
	self.entityListController.entities = document.model.entities;
}

- (void)windowDidLoad
{
	[super windowDidLoad];
	self.entityListController.delegate = self;
	self.entityDetailsController.delegate = self;
}

- (EntityListViewController *)entityListController
{
	return GDFDynamicCast(self.contentViewController.childViewControllers.firstObject, EntityListViewController);
}

- (EntityDetailsController *)entityDetailsController
{
	return GDFDynamicCast(self.contentViewController.childViewControllers[1], EntityDetailsController);
}

- (InspectorViewController *)inspectorController
{
	return GDFDynamicCast(self.contentViewController.childViewControllers.lastObject, InspectorViewController);
}

#pragma mark - EntityListViewControllerDelegate

- (void)entityListViewController:(EntityListViewController *)entityListController didSelectEntity:(NSEntityDescription *)entity
{
	self.entityDetailsController.entityDescription = entity;
	
	EntityInspectorViewController *entityInspector = [[EntityInspectorViewController alloc] init];
	entityInspector.entity = entity;
	self.inspectorController.inspectors = @[entityInspector];
}

#pragma mark - EntityDetailsControllerDelegate

- (void)entityDetailsController:(EntityDetailsController *)entityDetailsController didSelectPropertyDescription:(__kindof NSPropertyDescription *)propertyDescription
{
	NSMutableArray *inspectors = [[NSMutableArray alloc] init];
	
	EntityInspectorViewController *entityInspector = [[EntityInspectorViewController alloc] init];
	entityInspector.entity = self.entityDetailsController.entityDescription;
	[inspectors addObject:entityInspector];
	
	if (propertyDescription) {
		PropertyInspectorViewController *propertyInspector = [[PropertyInspectorViewController alloc] init];
		propertyInspector.property = propertyDescription;
		[inspectors addObject:propertyInspector];
	}
	
	self.inspectorController.inspectors = inspectors;
	
}

@end
